from django.shortcuts import render
from .forms import StatusForm
from .models import StatusModel
from datetime import datetime
from django.http import HttpResponseRedirect

# Create your views here.
def landingviews(request):
    statusForm = StatusForm(request.POST or None)
    if (request.method == 'POST' and statusForm.is_valid()) :
        simpan = StatusModel(
            status = statusForm.data['status'],
        )
        simpan.save()
        return HttpResponseRedirect('/')
    create_status = {
        'StatusForm' : statusForm,
        'listModel' : StatusModel.objects.all()
    }
    return render(request, 'landingpagehtml/landingpage.html', create_status)