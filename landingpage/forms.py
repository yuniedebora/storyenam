from django import forms
from datetime import datetime

class StatusForm(forms.Form):
    status = forms.CharField(max_length=300)