from django.test import TestCase, Client
from django.urls import resolve

from .views import landingviews
from .forms import StatusForm
from .models import StatusModel

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class unitTest(TestCase):
    def test_url_bisa_diakses (self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_url_panggil_landingpageviews (self):
        response = resolve('/')
        self.assertEqual(response.func, landingviews)
    
    def test_views_panggil_html_landingpage (self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingpagehtml/landingpage.html')

    def test_html_ada_forms_dari_django (self):
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn('<form ', content)
        self.assertIn('<table>', content)
        self.assertIn ('<button ', content)
    
 #   def test_form_status_punya_length_300 (self):
 #       form = StatusForm(status = 'status')
 #       self.assertIn('max_length=300', form)

    def test_ada_modelnya (self, status='halo'):
        return StatusModel.objects.create(status = status)

    def test_status_disimpen_di_model(self):
        status = StatusModel.objects.create(status = 'coba model')
        hitung_status = StatusModel.objects.all().count()
        self.assertEqual(hitung_status, 1)
    
    def test_key_modelnya(self):
        status = StatusModel.objects.create(status = 'key')
        self.assertEqual('key', status.__str__())
    
    def test_form_masuk_model(self):
        response = Client().post('', {'status' : 'hallo'})
        response = Client().get('')
        self.assertIn('hallo', response.content.decode())

class functionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
        super(functionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(functionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        status = selenium.find_element_by_id('id_status')
        status.send_keys('Coba coba')

        submit = selenium.find_element_by_name('submitButton')
        submit.send_keys(Keys.RETURN)
        self.assertIn('Coba coba', selenium.page_source)






