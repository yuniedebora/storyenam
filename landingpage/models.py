from django.db import models
from datetime import datetime

# Create your models here.
class StatusModel (models.Model) :
    status = models.CharField(max_length=300)
    time = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.status
